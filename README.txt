WET4 - Splash Page and Sever Error Pages Module

This module will set up the WET4 Splash page and Server Error pages, for use with the WET4 Theme.

This module can be thought of in two parts, the splash page and the server error pages.

To set up the splash page:

This module adds variables to the Site Information page (admin/config/system/site-information) that add a page title, URL for the homepage, and the url to a Terms and Conditions page to the Wet4 themed splash page.  The default Drupal "Default Front Page" variable is still active and can be used to set the URL for the English or French homepage.  This module will provide a splash page for www.domain.com/ only, if there is anything after the domain in the URL then the splash page will not be shown.

The Splash Page variables will need to be set up as multilingual variables so that the splash page can be bilingual and link to the English and French homepages and Terms and Conditions page.

Once these variables have been set to be multilingual then use the English/French language toggle provided at the top of the page to toggle between the English and French versions of these variables.


To set up the server error pages:

This modules adds variables to the Site Information page (admin/config/system/site-information) that add custom titles and error messages to 403 and 404 server error pages.  There is a toggle button that, when selected, will override the default Drupal error pages.  If not selected the default Drupal error pages will be used.  

The Server Error Page variables will need to be set up as multilingual variables to that the server error pages can be bilingual.

Once these variables have been set to be multilingual then use the English/French language toggle provided at the top of the page to toggle between the English and French versions of these variables.