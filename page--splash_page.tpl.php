<header role="banner">
  <div id="wb-bnr" class="container">
    <object id="gcwu-sig" type="image/svg+xml" tabindex="-1" role="img" data="/sites/all/themes/wet4/assets/sig-alt-en.svg" aria-label="Government of Canada"></object>
  </div>
</header>
<main role="main" property="mainContentOfPage" class="container">
  <div class="col-md-12">
    <h1 class="wb-inv">Language selection - Government of Canada - <?php print i18n_variable_get('splash_page_title','en'); ?> / <span lang="fr">Sélection de la langue -  gouvernement du Canada - <?php print i18n_variable_get('splash_page_title','fr'); ?></span></h1>
    <section class="col-md-6">
      <h2 class="h3 text-center"><?php print i18n_variable_get('splash_page_title','en'); ?></h2>
      <ul class="list-unstyled">
        <li><a class="btn btn-lg btn-primary btn-block" href="<?php print i18n_variable_get('splash_page_homepage_link','en'); ?>">English</a></li>
        <li><a class="btn btn-lg btn-default btn-block mrgn-tp-sm" href="<?php print i18n_variable_get('splash_page_terms_link','en'); ?>" rel="license">Terms and conditions of use</a></li>
      </ul>
  	</section>
    <section class="col-md-6" lang="fr">
      <h2 class="h3 text-center"><?php print i18n_variable_get('splash_page_title','fr'); ?></h2>
      <ul class="list-unstyled">
        <li><a class="btn btn-lg btn-primary btn-block" href="<?php print i18n_variable_get('splash_page_homepage_link','fr'); ?>">Français</a></li>
        <li><a class="btn btn-lg btn-default btn-block mrgn-tp-sm" href="<?php print i18n_variable_get('splash_page_terms_link','fr'); ?>" rel="license">Conditions régissant l'utilisation</a></li>
      </ul>
    </section>
  </div>
</main>
<footer role="contentinfo" class="container">
  <object id="wmms" type="image/svg+xml" tabindex="-1" role="img" data="/sites/all/themes/wet4/assets/wmms-alt.svg" aria-label="Symbol of the Government of Canada"></object>
</footer>