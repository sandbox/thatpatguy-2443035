<?php
$english_title = i18n_variable_get('wetkit_403error_page_title','en');
$french_title = i18n_variable_get('wetkit_403error_page_title','fr');
$english_body = i18n_variable_get('wetkit_403error_page_body','en');
$french_body = i18n_variable_get('wetkit_403error_page_body','fr');
?>
<header role="banner" class="container">
  <div id="wb-bnr" class="row">
    <div class="row">
      <div class="col-sm-6">
        <object id="gcwu-sig" type="image/svg+xml" tabindex="-1" role="img" data="/sites/all/themes/wet4/assets/sig-alt-en.svg" aria-label="Government of Canada"></object>
      </div>
      <div class="col-sm-6">
        <object id="wmms" type="image/svg+xml" tabindex="-1" role="img" data="/sites/all/themes/wet4/assets/wmms-alt.svg" aria-label="Symbol of the Government of Canada"></object>
      </div>
    </div>
  </div>
</header>

<?php if ($english_body == $french_body): ?>

  <main role="main" property="mainContentOfPage" class="container">
    <div class="row mrgn-tp-lg">
      <div class="col-md-12">
        <h1><span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span> <?php print $english_title; ?></h1>
        <?php print $english_body; ?>
      </div>
    </div>
  </main>

<?php else: ?>

  <main role="main" property="mainContentOfPage" class="container">
    <div class="row mrgn-tp-lg">
      <h1 class="wb-inv"><?php print $english_title; ?> / <span lang="fr"><?php print $french_title; ?></span></h1>
      <section class="col-md-6">
        <h2>
          <span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span>
          <?php print $english_title; ?>
        </h2>
        <?php print $english_body; ?>
      </section>
      <section class="col-md-6" lang="fr">
        <h2>
          <span class="glyphicon glyphicon-warning-sign mrgn-rght-md"></span>
          <?php print $french_title; ?>
        </h2>
        <?php print $french_body; ?>
      </section>
    </div>
  </main>

<?php endif; ?>